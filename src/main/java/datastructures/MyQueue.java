package datastructures;

import java.util.NoSuchElementException;

/**
 * Implementation of queue using two stacks.
 */
public class MyQueue {
    private int size;
    private MyStack pollStack;
    private MyStack removeStack;

    public MyQueue() {
        pollStack = new MyStack();
        removeStack = new MyStack();
    }

    public int size() {
        return this.size;
    }

    public void poll(Object data) {
        pollStack.push(data);
        size++;
    }

    public Object remove() {
        if (removeStack.size() == 0) {
            if (pollStack.size() == 0) {
                throw new NoSuchElementException();
            }

            while (pollStack.size() > 0) {
                removeStack.push(pollStack.pop());
            }
        }

        size--;
        return removeStack.pop();
    }

    public void printQueue() {
        System.out.println("remove stack:");
        removeStack.printStack();
        System.out.println("poll stack:");
        pollStack.printStack();
    }


}
