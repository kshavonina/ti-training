package datastructures;

public class Main {
    public static void main(String[] args) {
        QueueOnArray queueOnArray = new QueueOnArray(5);
        queueOnArray.poll(1);
        queueOnArray.poll(2);
        queueOnArray.poll(3);
        queueOnArray.poll(4);
        queueOnArray.poll(5);
        queueOnArray.remove();
        queueOnArray.remove();
        queueOnArray.poll(6);
        queueOnArray.poll(7);
        queueOnArray.poll(8);
        queueOnArray.remove();
        queueOnArray.poll(9);

        queueOnArray.printQueue();
        System.out.println("size: " + queueOnArray.size());
        System.out.println("array size: " + queueOnArray.getArraySize());
    }
}
