package datastructures;

import java.util.NoSuchElementException;

public class MyLinkedList {
    private int size;
    private Node lastNode;
    private Node firstNode;

    public int size() {
        return this.size;
    }

    /**
     * Insert element in the end of list.
     *
     * @param data
     */
    public void insert(Object data) {
        Node node = new Node(data);

        if (this.size == 0) {
            this.firstNode = node;
        } else {
            this.lastNode.nextNode = node;
        }

        this.lastNode = node;
        size++;
    }

    public void insert(int index, Object data) {
        if (this.size == 0) {
            if (index == 0) {
                this.insert(data);
            } else {
                throw new IllegalArgumentException("Incorrect index value. List is empty now.");
            }
        } else {
            Node node = new Node(data);
            node.nextNode = getNodeByIndex(index);

            Node nodeByPrevIndex = getNodeByIndex(index - 1);
            nodeByPrevIndex.nextNode = node;

            this.size++;
        }

    }

    /**
     * Removes the last element in list if exists.
     */
    public Object remove() {
        if (size == 0) {
            throw new NullPointerException();
        }

        Node retVal = getNodeByIndex(this.size - 1);

        Node nodeBeforeLast = getNodeByIndex(this.size - 2);
        nodeBeforeLast.nextNode = null;
        lastNode = nodeBeforeLast;
        this.size--;

        if (size == 0) {
            firstNode = null;
            lastNode = null;
        }

        return retVal.data;
    }

    public Object get(int index) {
        return getNodeByIndex(index).data;
    }

    private Node getNodeByIndex(int index) {
        Iterator iterator = new Iterator();

        for (int i = 0; i < index; i++) {
            iterator.next();
        }

        return iterator.next();
    }

    /**
     * Print all the elements of list.
     */
    public void printList() {
        Iterator iterator = new Iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next().data);
        }
    }

    private class Node {
        private Object data;
        private Node nextNode;

        private Node(Object data) {
            this.data = data;
        }
    }

    private class Iterator {
        private Node current = firstNode;

        private Iterator() {

        }

        private Node next() {
            if (this.hasNext()) {
                Node retVal = current;
                current = current.nextNode;
                return retVal;
            }

            throw new NoSuchElementException();
        }

        private boolean hasNext() {
            return current != null;
        }

    }

}
