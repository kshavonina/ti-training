package datastructures;

import java.util.NoSuchElementException;

public class MyStack {
    private int size;
    private MyLinkedList stack;

    public MyStack() {
        this.stack = new MyLinkedList();
    }

    public void push(Object data) {
        this.stack.insert(data);
        this.size++;
    }

    public Object pop() {
        if (this.size == 0) {
            throw new NoSuchElementException();
        }

        this.size--;
        return this.stack.remove();
    }

    public void printStack() {
        this.stack.printList();
    }

    public int size() {
        return this.size;
    }
}
