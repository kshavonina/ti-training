package datastructures;

public class TaskWithBrackets {
    public static void main(String[] args) {
        TaskWithBrackets taskWithBrackets = new TaskWithBrackets();

        String someCorrectText = "((()))[sf[sf]sfsf{f}fs]ee";
        String incorrectText = "(";
        String incorrectText2 = "]";


        System.out.println(taskWithBrackets.isExpressionCorrect(incorrectText2));

    }

    private boolean isExpressionCorrect(CharSequence text) {
        MyStack stack = new MyStack();

        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);

            if (ch == '[' || ch == '{' || ch == '(') {
                stack.push(ch);
            }

            if (ch == ']' || ch == '}' || ch == ')') {
                if (stack.size() == 0) {
                    return false;
                }

                char chPop = (char) stack.pop();

                if (ch == ')' && chPop != '('
                        || ch == '}' && chPop != '{'
                        || ch == ']' && chPop != '[') {
                    return false;
                }
            }
        }

        if (stack.size() > 0) {
            return false;
        }

        return true;
    }
}
