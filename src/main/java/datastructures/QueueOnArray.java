package datastructures;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class QueueOnArray {
    private int size;
    private int arraySize;
    private Object[] array;

    private int removeIndex;
    private int pollIndex;


    public QueueOnArray() {
        this(10);
    }

    public QueueOnArray(int capacity) {
        this.array = new Object[capacity];
        this.arraySize = capacity;
    }

    public boolean poll(Object data) {
        // 1 array is empty
        if (size == 0) {
            array[pollIndex] = data;
            pollIndex++;
            size++;
            return true;
        }

        // 2 array is full and we did not delete anything from queue
        if (pollIndex == arraySize && removeIndex == 0) {
            array = Arrays.copyOf(array, arraySize + arraySize / 2);
            arraySize = arraySize + arraySize / 2;
            removeIndex = 0;
            array[pollIndex] = data;
            pollIndex++;
            size++;
            return true;
        }

        // 3 array is full on right side but have empty 0 cell
        if (pollIndex == arraySize && removeIndex > 0/* && removeIndex != pollIndex*/) {
            array[0] = data;
            pollIndex = 1;
            size++;
            return true;
        }

        // 4 array is full on right side and we have no epmty cells at all
        if (pollIndex == removeIndex) {
            Object[] newArray = new Object[arraySize + arraySize / 2];

            int k = 0;

            for (int i = removeIndex; i < arraySize; i++, k++) {
                newArray[k] = array[i];
            }

            for (int i = 0; i < removeIndex; i++, k++) {
                newArray[k] = array[i];
            }

            newArray[arraySize] = data;
            size++;
            pollIndex = size;

            array = newArray;
            arraySize = arraySize + arraySize / 2;
            removeIndex = 0;
            return true;
        }

        // 5 array is full on right, 0 cell is not empty, but we have empty space in the middle of array
        /* pollIndex == arraySize && removeIndex > 0 && removeIndex > pollIndex */
        array[pollIndex] = data;
        pollIndex++;
        size++;
        return true;
    }

    public Object remove() {
        if (size == 0) {
            throw new NoSuchElementException();
        }

        if (removeIndex == arraySize) {
            Object retVal = array[0];
            array[0] = null;
            removeIndex = 1;
            size--;
            return retVal;
        }

        Object retVal = array[removeIndex];
        array[removeIndex] = null;
        removeIndex++;
        size--;
        return retVal;
    }

    public void printQueue() {
        for (int i = 0; i < arraySize; i++) {
            System.out.print(array[i] + "\t");
        }
        System.out.println();
    }

    public int size() {
        return this.size;
    }

    public int getArraySize() {
        return arraySize;
    }
}
